ARG CODENAME
FROM ubuntu:${CODENAME}

LABEL maintainer="Christian Pointner <equinox@spreadspace.org>"

ARG CODENAME
ARG APT_MIRROR=http://archive.ubuntu.com/ubuntu
ARG APT_MIRROR_SECURITY=http://archive.ubuntu.com/ubuntu
RUN set -x \
    && echo "deb ${APT_MIRROR} ${CODENAME} main universe" > /etc/apt/sources.list \
    && echo "deb ${APT_MIRROR} ${CODENAME}-updates main universe" >> /etc/apt/sources.list \
    && echo "deb ${APT_MIRROR_SECURITY} ${CODENAME}-security main universe" >> /etc/apt/sources.list \
    && echo 'APT::Install-Recommends "false";' > /etc/apt/apt.conf.d/02-norecommends \
    && rm -f /etc/apt/sources.list.d/ubuntu.sources \
    && apt-get update -q \
    && apt-get upgrade -y -q \
    && apt-get install -y -q tzdata locales curl ca-certificates \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY common/spreadspace-repo.gpg /etc/apt/trusted.gpg.d/spreadspace.gpg
RUN set -x \
    && echo "deb http://build.spreadspace.org ${CODENAME} main streaming" >>/etc/apt/sources.list.d/spreadspace.list \
    && apt-get update -q \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV TZ="Europe/Vienna"
RUN set -x \
    && echo "de_AT.UTF-8 UTF-8\nen_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && ln -snf "/usr/share/zoneinfo/${TZ}" /etc/localtime \
    && echo "${TZ}" > /etc/timezone

RUN adduser --home /srv --no-create-home --system --uid 990 --group app

ARG TINI_VERSION="v0.19.0"
ARG TINI_VARIANT="static-muslc-amd64"
COPY common/tini.checksums /tmp/tini.checksums
RUN set -ex \
    && cd /tmp \
    && curl -sfL -o "tini-${TINI_VARIANT}_${TINI_VERSION}" "https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-${TINI_VARIANT}" \
    && sha256sum --check --ignore-missing tini.checksums \
    && install --mode=0755 "tini-${TINI_VARIANT}_${TINI_VERSION}" /tini \
    && rm -rf /tmp/tini*

ENTRYPOINT ["/tini", "--"]
